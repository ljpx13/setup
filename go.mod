module gitlab.com/ljpx13/setup

go 1.14

require (
	gitlab.com/ljpx13/di v0.4.5
	gitlab.com/ljpx13/logging v0.1.2
	gitlab.com/ljpx13/test v0.1.6
)
