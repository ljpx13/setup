package setup

import "context"

// LongRunningService defines the methods that any long running service must
// implement.  If the Run method returns an error, the application will stop.
// The service must honor the cancellation of the provided context by returning
// a nil error.
type LongRunningService interface {
	Name() string
	Run(ctx context.Context) error
}
