package setup

// LongRunningServiceFunc is an alias for interface{} that is inteded to be used
// to represent NewT style functions that produce LongRunningService
// implementations.
type LongRunningServiceFunc interface{}
