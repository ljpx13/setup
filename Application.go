package setup

import (
	"context"

	"gitlab.com/ljpx13/di"
)

// Application defines the methods that any runtime application must implement.
// Services must return a slice of valid dependency injection functions.
type Application interface {
	Register(c di.Container)
	Services() []LongRunningServiceFunc
	BeforeStart(ctx context.Context, c di.ReadOnlyContainer) error
	AfterStart(ctx context.Context, c di.ReadOnlyContainer) error
}
