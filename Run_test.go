package setup

import (
	"bytes"
	"context"
	"errors"
	"io"
	"testing"
	"time"

	"gitlab.com/ljpx13/di"
	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/has"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestRunEndsWhenContextCancelled(t *testing.T) {
	// Arrange.
	ctx, cancel := context.WithCancel(context.Background())

	// Act.
	go func() {
		time.Sleep(time.Millisecond * 100)
		cancel()
	}()
	errs := start(ctx, cancel, &testApplication{})

	// Assert.
	test.That(t, errs, has.Length(1))
	test.That(t, errs[0].Error(), is.EqualTo("finished"))
}

func TestRunEndsWhenBeforeStartFails(t *testing.T) {
	// Arrange.
	ctx, cancel := context.WithCancel(context.Background())

	// Act.
	errs := start(ctx, cancel, &testApplication{beforeStartFails: true})

	// Assert.
	test.That(t, errs, has.Length(1))
	test.That(t, errs[0].Error(), is.EqualTo("failed before"))
}

func TestRunEndsWhenAfterStartFails(t *testing.T) {
	// Arrange.
	ctx, cancel := context.WithCancel(context.Background())

	// Act.
	errs := start(ctx, cancel, &testApplication{afterStartFails: true})

	// Assert.
	test.That(t, errs, has.Length(2))
	test.That(t, errs[0].Error(), is.EqualTo("failed after"))
	test.That(t, errs[1].Error(), is.EqualTo("finished"))
}

func TestRunAllServicesShutdownWhenOneEnds(t *testing.T) {
	// Arrange.
	ctx, cancel := context.WithCancel(context.Background())

	// Act.
	errs := start(ctx, cancel, &testApplication{useTestService2: true})

	// Assert.
	test.That(t, errs, has.Length(2))
	test.That(t, errs[0].Error(), is.EqualTo("service faulted"))
	test.That(t, errs[1].Error(), is.EqualTo("finished"))
}

func TestRunWithServiceThatIsNotLongRunningService(t *testing.T) {
	// Arrange.
	ctx, cancel := context.WithCancel(context.Background())

	// Act.
	errs := start(ctx, cancel, &testApplication{useNonService: true})

	// Assert.
	test.That(t, errs, has.Length(1))
	test.That(t, errs[0].Error(), is.EqualTo("type '*bytes.Buffer' is not a LongRunningService"))
}

// -----------------------------------------------------------------------------

type testApplication struct {
	beforeStartFails bool
	afterStartFails  bool
	useTestService2  bool
	useNonService    bool
}

var _ Application = &testApplication{}

func (a *testApplication) Register(c di.Container) {}

func (a *testApplication) Services() []LongRunningServiceFunc {
	services := []LongRunningServiceFunc{newTestService1}

	if a.useTestService2 {
		services = append(services, newTestService2)
	}

	if a.useNonService {
		services = append(services, func() io.Reader { return bytes.NewBufferString("") })
	}

	return services
}

func (a *testApplication) BeforeStart(ctx context.Context, c di.ReadOnlyContainer) error {
	if a.beforeStartFails {
		return errors.New("failed before")
	}

	return nil
}

func (a *testApplication) AfterStart(ctx context.Context, c di.ReadOnlyContainer) error {
	if a.afterStartFails {
		return errors.New("failed after")
	}

	return nil
}

type testService1 struct {
	ctx context.Context
}

var _ LongRunningService = &testService1{}

func newTestService1(ctx context.Context) *testService1 {
	return &testService1{
		ctx: ctx,
	}
}

func (s *testService1) Name() string {
	return "testService1"
}

func (s *testService1) Run(ctx context.Context) error {
	<-ctx.Done()
	return errors.New("finished")
}

type testService2 struct{}

var _ LongRunningService = &testService2{}

func newTestService2() *testService2 {
	return &testService2{}
}

func (s *testService2) Name() string {
	return "testService2"
}

func (s *testService2) Run(ctx context.Context) error {
	time.Sleep(time.Millisecond * 100)
	return errors.New("service faulted")
}
