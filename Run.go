package setup

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"reflect"
	"sync"
	"syscall"

	"gitlab.com/ljpx13/di"
	"gitlab.com/ljpx13/logging"
)

// Run runs the provided application.
func Run(application Application) {
	ctx, cancel := context.WithCancel(context.Background())

	sigc := make(chan os.Signal, 1)
	signal.Notify(sigc, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sigc
		cancel()
	}()

	errs := start(ctx, cancel, application)
	if len(errs) > 0 {
		for _, err := range errs {
			fmt.Fprintf(os.Stderr, "× %v\n", err)
		}

		os.Exit(1)
	}

	os.Exit(0)
}

func start(ctx context.Context, cancel context.CancelFunc, application Application) []error {
	c := di.NewContainer()
	c.Register(di.AnchoredSingleton, func() context.Context { return ctx })
	application.Register(c)

	var logger logging.Logger
	err := c.Resolve(&logger)
	if err != nil {
		logger = log.New(os.Stdout, "", 0)
		c.Register(di.Singleton, func() logging.Logger {
			return logger
		})
	}

	err = application.BeforeStart(ctx, c)
	if err != nil {
		return []error{err}
	}

	services, err := setupServices(application, c)
	if err != nil {
		return []error{err}
	}

	if len(services) > 0 {
		logger.Printf("↑↑↑\n")
		defer logger.Printf("↓↓↓\n")
	}

	errc := make(chan error)
	donec := make(chan struct{})
	wg := &sync.WaitGroup{}
	wg.Add(len(services))
	go func() {
		wg.Wait()
		close(donec)
	}()

	for _, service := range services {
		go func(service LongRunningService) {
			serviceName := service.Name()
			logger.Printf("[%v] ↑\n", serviceName)
			errc <- service.Run(ctx)
			logger.Printf("[%v] ↓\n", serviceName)
			wg.Done()
		}(service)
	}

	errors := []error{}

	err = application.AfterStart(ctx, c)
	if err != nil {
		errors = append(errors, err)
		cancel()
	}

	for {
		select {
		case err := <-errc:
			if err == nil {
				continue
			}

			cancel()
			errors = append(errors, err)
		case <-donec:
			return errors
		}
	}
}

func setupServices(application Application, c di.Container) ([]LongRunningService, error) {
	services := []LongRunningService{}

	for _, serviceFunc := range application.Services() {
		serviceType := c.Register(di.Singleton, serviceFunc)
		serviceInterface, err := c.ResolveType(serviceType)
		if err != nil {
			return nil, err
		}

		service, ok := serviceInterface.(LongRunningService)
		if !ok {
			return nil, fmt.Errorf("type '%v' is not a LongRunningService", reflect.TypeOf(serviceInterface))
		}

		services = append(services, service)
	}

	return services, nil
}
